-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : sam. 19 juin 2021 à 12:28
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `formulaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `recceuil`
--

CREATE TABLE `recceuil` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `naissance` datetime NOT NULL,
  `telephone` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pays` varchar(255) NOT NULL,
  `photo_profil` varchar(255) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `recceuil`
--

INSERT INTO `recceuil` (`id`, `nom`, `prenom`, `sexe`, `naissance`, `telephone`, `email`, `pays`, `photo_profil`, `mot_de_passe`) VALUES
(68, 'cedric', 'cedric', '', '2021-06-01 00:00:00', 5555555, 'piotie@gmail.com', '', 'Submit', '1111111'),
(69, 'cedric', 'cedric', '', '2021-06-02 00:00:00', 555555555, 'epiotie2@gmail.com', '', 'Capture d’écran 2021-06-17 000641.png', '111111111'),
(70, 'cedric', 'cedric', '', '2021-06-02 00:00:00', 555555555, 'pepiotie2@gmail.com', '', 'Capture d’écran 2021-06-17 000641.png', '00000');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `recceuil`
--
ALTER TABLE `recceuil`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `recceuil`
--
ALTER TABLE `recceuil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
