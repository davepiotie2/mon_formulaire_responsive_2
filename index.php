<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"> -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="bootstrap-3/css/bootstrap.css">
    <!-- <link rel="stylesheet" href="bootstrap-4/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="bootstrap-5/css/bootstrap.min.css"> -->

<link rel="stylesheet" href="css/style.css">
<title>Title</title>

</head>
<body  style="background-image:url(images/img2.jpg);">
  

    


<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xl-offset-3 col-xl-6 bg-info" style="margin-top:10px;margin-bottom:10px; border:1px solid black;">  
<form method="post" action="traitement.php">
  <label class="bg-primary" style="border-color: #46b8da;font-weight:800;color:white;font-size:15px;width:620px;height:40px;text-align:center;text-decoration:underline;">Formulaire de recueil</label><br><br>
  
  <label style="text-decoration:underline;" > Nom</label>
    <div class='input-group'>
        <span class='input-group-addon'>
            <span class='fa fa-user'></span>
        </span>
        <input class="form-control" type="text" id="nom" name="nom" placeholder="Nom" pattern="[a-zA-ZÀ-ÿ]{4,20}" minlength="4" required  >
      </div>
      <!-- <small id="p_nom">**Un mininum de 4 lettres est requis.**</small><br> -->
      
      <label style="text-decoration:underline;">Prénom</label>
      <div class='input-group' >
        <span class='input-group-addon'>
          <span class='fa fa-user'></span>
        </span>
        <input class="form-control" type="text" id="prenom" name="prenom" placeholder="Prenom" pattern="[a-zA-ZÀ-ÿ]{4,20}" minlength="4" required  >
      </div>
      <!-- <small id="p_prenom">**Un mininum de 4 lettres est requis.**</small><br> -->
    
       
       
       <label >Sexe</label>
       <div class='input-group'>
        <span class='input-group-addon'>
            <span class='fa fa-transgender'></span>
        </span>
       <select name="sexe"  class="form-control" required >
        <option value="" selected disabled> Gendre</option>
        <option value="masculin">Masculin</option>
        <option value="feminin">Féminin</option>
      </select>
      </div>
      
      
      
      <label style="text-decoration:underline;">Date de naissance</label>
      <div class='input-group'>
        <span class='input-group-addon'>
          <span class='glyphicon glyphicon-time'></span>
        </span>
        <input class="form-control" type="date" name="naissance" placeholder="Age" required >
      </div>
      
      
      
      <label style="text-decoration:underline;">Email</label>
      <div class='input-group' >
        <span class='input-group-addon'>
          <span class='fa fa-envelope'></span>
        </span>
        <input class="form-control" type="email" name="email" placeholder="Your Email" required >
      </div>
			<!-- <p class="alert">**Votre Email n'est pas conforme.**</p>  -->
      
    
    <label style="text-decoration:underline;">Mot de passe</label>
    <div class='input-group' >
        <span class='input-group-addon'>
            <span class='fa fa-lock'></span>
        </span>    
        <input class="form-control" type="password" name="mot_de_passe" placeholder="Mot de passe" required >
    </div>
    
    
    
    
    <label style="text-decoration:underline;">Choix du pays</label>
    <div class='input-group'>
        <span class='input-group-addon'>
            <span class='fa fa-flag'></span>
        </span>    
      <select name="pays" id="" class="form-control" required >
        <option value="" selected disabled>Choix du pays</option>
        <option value="cameroun">Cameroun</option>
        <option value="senegal">Senegal</option>
        <option value="benin">Benin</option>
        <option value="gabon">Gabon</option>
        <option value="maroc">Maroc</option>
        <option value="tunisie">Tunisie</option>
        <option value="maroc">Maroc</option>
        <option value="algerie">Algerie</option>
        <option value="egypte">Egypte</option>
        <option value="lybie">Lybie</option>
        <option value="soudan">Soudan</option>
        <option value="tchad">Tchad</option>
        <option value="afrique du sud">Afrique du Sud</option>
      </select>
      </div>
        <body>
  <!-- <h1>International Telephone Input</h1>
  <form>
    <input id="phone" name="phone" type="tel">
    <button type="submit">Submit</button>
  </form>

  <script src="build/js/intlTelInput.js"></script>
  <script>
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
       separateDialCode: true,
      utilsScript: "build/js/utils.js",
    });
  </script>
</body> -->
   
    <label style="text-decoration:underline;">Numéro de téléphone</label>
    <div class='input-group'>
        <span class='input-group-addon'>
            <span class='fa fa-phone'></span>
        </span>    
        <input class="form-control" type="tel" name="telephone" placeholder="Téléphone" required  pattern="[0-9]+" minlength="9" maxlength="20">
    </div>
    
  <label for="photo" style="text-decoration:underline;">Photo de profil</label>
  <input type="file" name="photo_profil" id="photo" class="form-control" accept="image/*" onchange="loadFile(event)" required><br>
  <div class="col-md-offset-4 col-md-4" style="margin-top: 10px; margin-bottom: 10px; height: 130px; width: 130px; border-radius: 50%;">
  <img    id="pp" style="height: 130px; width: 130px; border-radius: 50%;"  >
  </div>
  
  <input  type="submit" class="btn btn-block btn-primary" style="text-decoration:underline !important" >
  </form>
</div>





  



<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="jquery/jquery.min.js"></script>
<script src="bootstrap-3/js/bootstrap.min.js"></script>
<!-- <script src="bootstrap-4/js/bootstrap.min.js"></script> -->
<!-- <script src="bootstrap-5/js/bootstrap.min.js"></script> -->
<script type="text/javascript">
// var nom=document.getElementsById('nom');

// nom.addEventListener('change', testnom);
// //console.log(nom.value);
// function testnom(){
// 		// console.log(nom.value);
// 			if(nom.value.length<4){
// 				p_nom.style.display='block';
// 				console.log("longeur du nom "+nom.value.length);
// 				return false;
// 			}
// 			else{
// 				console.log("longeur du nom "+nom.value.length+" ok");
// 				p_nom.style.display='none';
// 				nom.style.backgroundColor='rgb(232,240,254)';
// 				return true;
// 			}
// 		}
                
              
        
    
    
// prenom.addEventListener('change', testprenom);
// 		// console.log(prenom);
// 		function testprenom(){
// 			if(prenom.value.length<4){
// 				p_prenom.style.display='block';
// 				return false;
// 			}
// 			else{
// 				p_prenom.style.display='none';
// 				prenom.style.backgroundColor='rgb(232,240,254)';
// 				return true;
// 			}
// 		}

//     mail.addEventListener('change', testmail);
// 		// console.log(mail);
// 		function testmail(){
// 			var mail_indic='0';
// 			var mail_format=[],mail_ending=[];
// 			mail_format=['gmail.com','yahoo.com','yahoo.fr','hotmail.com'];
// 			var mail_ok;
// 			// console.log('test');
// 			for(var i=0; i<mail.value.length; i++){
// 				if(mail.value[i]=='@'){
// 					mail_indic=mail.value[i];
// 				}
// 			}
// 			// console.log(mail_indic);
// 			if(mail_indic=='@'){
// 				mail_ending=mail.value.split('@');
// 				// console.log(mail_ending);
// 				for (var i = 0; i < mail_format.length; i++) {
// 					// console.log(mail_format[i]+' ?= '+mail_ending[1]);
// 					if(mail_format[i]==mail_ending[1]){
// 						mail_ok=true;
// 						break;
// 					}
// 					else{
// 						mail_ok=false;
// 					}
// 				}
// 			}
// 			else{
// 				mail_ok=false;
// 			}

// 			if(mail_ok==true){
// 				p_mail.style.display='none';
// 				mail.style.backgroundColor='rgb(232,240,254)';
// 				return true;
// 			}
// 			else{
// 				p_mail.style.display='block';
// 				return false;
// 			}
// 		}
    var loadFile = function(event) {
        var profil = document.getElementById('pp');
        profil.src = URL.createObjectURL(event.target.files[0]);
      };
    

</script>        
</body>
</html>